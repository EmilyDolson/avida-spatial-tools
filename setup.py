from distutils.core import setup
__author__ = "Emily Dolson"
__copyright__ = "Copyright 2014, Emily Dolson"
__email__ = "EmilyLDolson@gmail.com"
__status__ = "Development"


setup(name='avidaspatial',
      version='1.0',
      description="Tools for working with spatial in Avida",
      author="Emily Dolson",
      author_email="EmilyLDolson@gmail.com",
      url="https://github.com/emilydolson/avida-spatial-tools",
      packages=["avidaspatial"],
      keywords = ["avida"],
      download_url = "https://github.com/emilydolson/avida-spatial-tools/tarball/1.0",
      )
